<?php

/**
 * The user hasn't started sending yet.
 */
namespace Mandrill\Exceptions;
class NoSendingHistory extends MandrillError
{
}