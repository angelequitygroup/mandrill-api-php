<?php

/**
 * The requested URL has not been seen in a tracked link
 */
namespace Mandrill\Exceptions;
class UnknownUrl extends MandrillError
{
}