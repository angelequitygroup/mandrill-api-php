<?php

use Mandrill\Exceptions\HttpError;
use Mandrill\Exceptions\MandrillError;

require_once 'Mandrill/Templates.php';
require_once 'Mandrill/Exports.php';
require_once 'Mandrill/Users.php';
require_once 'Mandrill/Rejects.php';
require_once 'Mandrill/Inbound.php';
require_once 'Mandrill/Tags.php';
require_once 'Mandrill/Messages.php';
require_once 'Mandrill/Whitelists.php';
require_once 'Mandrill/Ips.php';
require_once 'Mandrill/Internal.php';
require_once 'Mandrill/Subaccounts.php';
require_once 'Mandrill/Urls.php';
require_once 'Mandrill/Webhooks.php';
require_once 'Mandrill/Senders.php';
require_once 'Mandrill/Metadata.php';
require_once 'Mandrill/Exceptions/MandrillError.php';
require_once 'Mandrill/Exceptions/HttpError.php';
require_once 'Mandrill/Exceptions/ValidationError.php';
require_once 'Mandrill/Exceptions/InvalidKey.php';
require_once 'Mandrill/Exceptions/PaymentRequired.php';
require_once 'Mandrill/Exceptions/UnknownSubaccount.php';
require_once 'Mandrill/Exceptions/UnknownTemplate.php';
require_once 'Mandrill/Exceptions/ServiceUnavailable.php';
require_once 'Mandrill/Exceptions/UnknownMessage.php';
require_once 'Mandrill/Exceptions/InvalidTagName.php';
require_once 'Mandrill/Exceptions/InvalidReject.php';
require_once 'Mandrill/Exceptions/UnknownSender.php';
require_once 'Mandrill/Exceptions/UnknownUrl.php';
require_once 'Mandrill/Exceptions/UnknownTrackingDomain.php';
require_once 'Mandrill/Exceptions/InvalidTemplate.php';
require_once 'Mandrill/Exceptions/UnknownWebhook.php';
require_once 'Mandrill/Exceptions/UnknownInboundDomain.php';
require_once 'Mandrill/Exceptions/UnknownInboundRoute.php';
require_once 'Mandrill/Exceptions/UnknownExport.php';
require_once 'Mandrill/Exceptions/IPProvisionLimit.php';
require_once 'Mandrill/Exceptions/UnknownPool.php';
require_once 'Mandrill/Exceptions/NoSendingHistory.php';
require_once 'Mandrill/Exceptions/PoorReputation.php';
require_once 'Mandrill/Exceptions/UnknownIP.php';
require_once 'Mandrill/Exceptions/InvalidEmptyDefaultPool.php';
require_once 'Mandrill/Exceptions/InvalidDeleteDefaultPool.php';
require_once 'Mandrill/Exceptions/InvalidDeleteNonEmptyPool.php';
require_once 'Mandrill/Exceptions/InvalidCustomDNS.php';
require_once 'Mandrill/Exceptions/InvalidCustomDNSPending.php';
require_once 'Mandrill/Exceptions/MetadataFieldLimit.php';
require_once 'Mandrill/Exceptions/UnknownMetadataField.php';


class Mandrill {
    
    public $apikey;
    public $ch;
    public $root = 'https://mandrillapp.com/api/1.0';
    public $debug = false;

    public static $error_map = array(
        "ValidationError" => "Mandrill/Exceptions/ValidationError",
        "Invalid_Key" => "Mandrill/Exceptions/InvalidKey",
        "PaymentRequired" => "Mandrill/Exceptions/PaymentRequired",
        "Unknown_Subaccount" => "Mandrill/Exceptions/UnknownSubaccount",
        "Unknown_Template" => "Mandrill/Exceptions/UnknownTemplate",
        "ServiceUnavailable" => "Mandrill/Exceptions/ServiceUnavailable",
        "Unknown_Message" => "Mandrill/Exceptions/UnknownMessage",
        "Invalid_Tag_Name" => "Mandrill/Exceptions/InvalidTagName",
        "Invalid_Reject" => "Mandrill/Exceptions/InvalidReject",
        "Unknown_Sender" => "Mandrill/Exceptions/UnknownSender",
        "Unknown_Url" => "Mandrill/Exceptions/UnknownUrl",
        "Unknown_TrackingDomain" => "Mandrill/Exceptions/UnknownTrackingDomain",
        "Invalid_Template" => "Mandrill/Exceptions/InvalidTemplate",
        "Unknown_Webhook" => "Mandrill/Exceptions/UnknownWebhook",
        "Unknown_InboundDomain" => "Mandrill/Exceptions/UnknownInboundDomain",
        "Unknown_InboundRoute" => "Mandrill/Exceptions/UnknownInboundRoute",
        "Unknown_Export" => "Mandrill/Exceptions/UnknownExport",
        "IP_ProvisionLimit" => "Mandrill/Exceptions/IPProvisionLimit",
        "Unknown_Pool" => "Mandrill/Exceptions/UnknownPool",
        "NoSendingHistory" => "Mandrill/Exceptions/NoSendingHistory",
        "PoorReputation" => "Mandrill/Exceptions/PoorReputation",
        "Unknown_IP" => "Mandrill/Exceptions/UnknownIP",
        "Invalid_EmptyDefaultPool" => "Mandrill/Exceptions/InvalidEmptyDefaultPool",
        "Invalid_DeleteDefaultPool" => "Mandrill/Exceptions/InvalidDeleteDefaultPool",
        "Invalid_DeleteNonEmptyPool" => "Mandrill/Exceptions/InvalidDeleteNonEmptyPool",
        "Invalid_CustomDNS" => "Mandrill/Exceptions/InvalidCustomDNS",
        "Invalid_CustomDNSPending" => "Mandrill/Exceptions/InvalidCustomDNSPending",
        "Metadata_FieldLimit" => "Mandrill/Exceptions/MetadataFieldLimit",
        "Unknown_MetadataField" => "Mandrill/Exceptions/UnknownMetadataField"
    );

    public function __construct($apikey=null) {
        if(!$apikey) $apikey = getenv('MANDRILL_APIKEY');
        if(!$apikey) $apikey = $this->readConfigs();
        if(!$apikey) throw new MandrillError('You must provide a Mandrill API key');
        $this->apikey = $apikey;

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mandrill-PHP/1.0.55');
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);

        $this->root = rtrim($this->root, '/') . '/';

        $this->templates = new \Mandrill\Templates($this);
        $this->exports = new \Mandrill\Exports($this);
        $this->users = new \Mandrill\Users($this);
        $this->rejects = new \Mandrill\Rejects($this);
        $this->inbound = new \Mandrill\Inbound($this);
        $this->tags = new \Mandrill\Tags($this);
        $this->messages = new \Mandrill\Messages($this);
        $this->whitelists = new \Mandrill\Whitelists($this);
        $this->ips = new \Mandrill\Ips($this);
        $this->internal = new \Mandrill\Internal($this);
        $this->subaccounts = new \Mandrill\Subaccounts($this);
        $this->urls = new \Mandrill\Urls($this);
        $this->webhooks = new \Mandrill\Webhooks($this);
        $this->senders = new \Mandrill\Senders($this);
        $this->metadata = new \Mandrill\Metadata($this);
    }

    public function __destruct() {
        curl_close($this->ch);
    }

    public function call($url, $params) {
        $params['key'] = $this->apikey;
        $params = json_encode($params);
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL, $this->root . $url . '.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);

        $start = microtime(true);
        $this->log('Call to ' . $this->root . $url . '.json: ' . $params);
        if($this->debug) {
            $curl_buffer = fopen('php://memory', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $curl_buffer);
        }

        $response_body = curl_exec($ch);
        $info = curl_getinfo($ch);
        $time = microtime(true) - $start;
        if($this->debug) {
            rewind($curl_buffer);
            $this->log(stream_get_contents($curl_buffer));
            fclose($curl_buffer);
        }
        $this->log('Completed in ' . number_format($time * 1000, 2) . 'ms');
        $this->log('Got response: ' . $response_body);

        if(curl_error($ch)) {
            throw new HttpError("API call to $url failed: " . curl_error($ch));
        }
        $result = json_decode($response_body, true);
        if($result === null) throw new MandrillError('We were unable to decode the JSON response from the Mandrill API: ' . $response_body);
        
        if(floor($info['http_code'] / 100) >= 4) {
            throw $this->castError($result);
        }

        return $result;
    }

    public function readConfigs() {
        $paths = array('~/.mandrill.key', '/etc/mandrill.key');
        foreach($paths as $path) {
            if(file_exists($path)) {
                $apikey = trim(file_get_contents($path));
                if($apikey) return $apikey;
            }
        }
        return false;
    }

    public function castError($result) {
        if($result['status'] !== 'error' || !$result['name']) throw new MandrillError('We received an unexpected error: ' . json_encode($result));

        $class = (isset(self::$error_map[$result['name']])) ? self::$error_map[$result['name']] : 'MandrillError';
        return new $class($result['message'], $result['code']);
    }

    public function log($msg) {
        if($this->debug) error_log($msg);
    }
}


