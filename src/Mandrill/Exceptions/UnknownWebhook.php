<?php

/**
 * The requested webhook does not exist
 */
namespace Mandrill\Exceptions;
class UnknownWebhook extends MandrillError
{
}