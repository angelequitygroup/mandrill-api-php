<?php

/**
 * The provided message id does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownMessage extends MandrillError
{
}