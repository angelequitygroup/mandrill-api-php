<?php

/**
 * The provided API key is not a valid Mandrill API key
 */
namespace Mandrill\Exceptions;
class InvalidKey extends MandrillError
{
}