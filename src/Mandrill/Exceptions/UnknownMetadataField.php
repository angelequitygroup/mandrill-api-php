<?php

/**
 * The provided metadata field name does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownMetadataField extends MandrillError
{
}