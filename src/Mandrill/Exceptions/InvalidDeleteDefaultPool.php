<?php

/**
 * The default pool cannot be deleted.
 */
namespace Mandrill\Exceptions;
class InvalidDeleteDefaultPool extends MandrillError
{
}