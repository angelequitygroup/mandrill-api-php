<?php

/**
 * The provided dedicated IP does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownIP extends MandrillError
{
}