<?php

/**
 * The provided inbound route does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownInboundRoute extends MandrillError
{
}