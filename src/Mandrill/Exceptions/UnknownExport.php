<?php

/**
 * The requested export job does not exist
 */
namespace Mandrill\Exceptions;
class UnknownExport extends MandrillError
{
}