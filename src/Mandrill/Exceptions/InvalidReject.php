<?php

/**
 * The requested email is not in the rejection list
 */
namespace Mandrill\Exceptions;
class InvalidReject extends MandrillError
{
}