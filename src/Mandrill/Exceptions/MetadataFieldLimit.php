<?php

/**
 * Custom metadata field limit reached.
 */
namespace Mandrill\Exceptions;
class MetadataFieldLimit extends MandrillError
{
}