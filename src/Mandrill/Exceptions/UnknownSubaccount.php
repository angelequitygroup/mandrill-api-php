<?php

/**
 * The provided subaccount id does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownSubaccount extends MandrillError
{
}