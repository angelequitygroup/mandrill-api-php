<?php

/**
 * You cannot remove the last IP from your default IP pool.
 */
namespace Mandrill\Exceptions;
class InvalidEmptyDefaultPool extends MandrillError
{
}