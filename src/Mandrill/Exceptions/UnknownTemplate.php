<?php

/**
 * The requested template does not exist
 */
namespace Mandrill\Exceptions;
class UnknownTemplate extends MandrillError
{
}