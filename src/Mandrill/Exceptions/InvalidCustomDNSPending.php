<?php

/**
 * A custom DNS change for this dedicated IP is currently pending.
 */
namespace Mandrill\Exceptions;
class InvalidCustomDNSPending extends MandrillError
{
}