<?php

/**
 * The parameters passed to the API call are invalid or not provided when required
 */
namespace Mandrill\Exceptions;
class ValidationError extends MandrillError
{
}