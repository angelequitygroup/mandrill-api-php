<?php

/**
 * Non-empty pools cannot be deleted.
 */
namespace Mandrill\Exceptions;
class InvalidDeleteNonEmptyPool extends MandrillError
{
}