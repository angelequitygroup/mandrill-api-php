<?php

/**
 * The domain name is not configured for use as the dedicated IP's custom reverse DNS.
 */
namespace Mandrill\Exceptions;
class InvalidCustomDNS extends MandrillError
{
}