<?php

/**
 * The given template name already exists or contains invalid characters
 */
namespace Mandrill\Exceptions;
class InvalidTemplate extends MandrillError
{
}