<?php

/**
 * The requested tag does not exist or contains invalid characters
 */
namespace Mandrill\Exceptions;
class InvalidTagName extends MandrillError
{
}