<?php

/**
 * The provided dedicated IP pool does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownPool extends MandrillError
{
}