<?php

/**
 * The requested feature requires payment.
 */
namespace Mandrill\Exceptions;
class PaymentRequired extends MandrillError
{
}