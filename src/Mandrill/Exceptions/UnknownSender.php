<?php

/**
 * The requested sender does not exist
 */
namespace Mandrill\Exceptions;
class UnknownSender extends MandrillError
{
}