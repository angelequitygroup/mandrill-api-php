<?php

/**
 * The subsystem providing this API call is down for maintenance
 */
namespace Mandrill\Exceptions;
class ServiceUnavailable extends MandrillError
{
}