<?php

/**
 * A dedicated IP cannot be provisioned while another request is pending.
 */
namespace Mandrill\Exceptions;
class IPProvisionLimit extends MandrillError
{
}