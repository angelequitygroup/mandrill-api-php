<?php

/**
 * The user's reputation is too low to continue.
 */
namespace Mandrill\Exceptions;
class PoorReputation extends MandrillError
{
}