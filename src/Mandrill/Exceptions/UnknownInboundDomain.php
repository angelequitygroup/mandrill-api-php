<?php

/**
 * The requested inbound domain does not exist
 */
namespace Mandrill\Exceptions;
class UnknownInboundDomain extends MandrillError
{
}