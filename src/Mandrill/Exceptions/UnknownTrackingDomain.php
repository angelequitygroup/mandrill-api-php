<?php

/**
 * The provided tracking domain does not exist.
 */
namespace Mandrill\Exceptions;
class UnknownTrackingDomain extends MandrillError
{
}